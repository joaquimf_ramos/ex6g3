/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.ex6g3;

import java.util.Scanner;

/**
 *
 * @author Filipe Ramos
 */
public class ficha5G3 {

    public static void main(String[] args) {
        int n;
        int[] vec_n;
        int iMin = 999999999;
        int iMax = 0;
        int pos = 0;

        Scanner ler = new Scanner(System.in);

        System.out.println("Insira tamanho para o seu vetor.");

        n = ler.nextInt();
        vec_n = new int[n];

        for (int i = 0; i < n; i++) {
            System.out.println("Insira um numero no seu vetor?");

            vec_n[i] = ler.nextInt();

            if (vec_n[i] < iMin) {
                iMin = vec_n[i];
                pos = i;

                if (vec_n[i] > iMax) {
                    iMax = vec_n[i];
                    pos = i;
                }
            }
        }
        for (int i = 0; i < n; i++) {
            System.out.print(vec_n[i] + " | ");
        }
        System.out.print("O menor elemento de N e = " + iMin + " e a sua posicao e " + pos + ".");

        for (int i = n - 1; i >= 0; i--) {
            System.out.println(vec_n[i] + " | ");
        }

    }
}
